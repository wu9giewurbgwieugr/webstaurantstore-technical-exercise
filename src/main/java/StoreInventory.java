import java.util.HashMap;
import java.util.Map;

public class StoreInventory {
	private Map<Integer, Product> product;

	StoreInventory() {
		product = new HashMap<>();
	}

	public Map<Integer, Product> getProducts() {
		return product;
	}

	public void addProduct(String name, int id, double price, int quantity) {
		product.put(id, new Product(name, id, price, quantity));
	}

	public void removeProduct(int id) {
		product.remove(id);
	}

	public void updateQuantity(int id, int quantity) {
		Product existingProduct = product.get(id);
		if (existingProduct != null) {
			existingProduct.newQuantity(quantity);
		}
	}

	public String viewCurrentInventory() {
		StringBuilder buildProduct = new StringBuilder();
		if (product.isEmpty()) {
			return ("There are no products in the inventory.");
		} else {
			for (Product products : product.values()) {
				buildProduct.append(products.toString());
			}
			return buildProduct.toString();
		}
	}

	public double calculateTotalValue() {
		double total = 0.0;
		for (Product products : product.values()) {
			total += products.getPrice() * products.getQuantity();
		}
		return total;
	}
}
