import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StoreInventoryTest {

	public static final String NAME = "Product Name";
	public static final int ID = 1234;
	public static final double PRICE = 99.99;
	public static final int QUANTITY = 5;
	public static final int NEW_QUANTITY = 10;
	StoreInventory storeInventory;

	@BeforeEach
	void setUp() {
		storeInventory = new StoreInventory();
		Product product = new Product(NAME, ID, PRICE, QUANTITY);
	}

	@Test
	void store_inventory_initially_has_no_products() {
		assertTrue(storeInventory.getProducts().isEmpty());
	}

	@Test
	void add_product_to_store_inventory() {
		storeInventory.addProduct(NAME, ID, PRICE, QUANTITY);
		assertEquals(ID, storeInventory.getProducts().get(ID).getId());
	}

	@Test
	void remove_product_from_store_inventory() {
		storeInventory.addProduct(NAME, ID, PRICE, QUANTITY);
		storeInventory.removeProduct(ID);
		assertNull(storeInventory.getProducts().get(ID));
	}

	@Test
	void update_quantity_to_a_product() {
		storeInventory.addProduct(NAME, ID, PRICE, QUANTITY);
		storeInventory.updateQuantity(ID, NEW_QUANTITY);
		assertEquals(NEW_QUANTITY, storeInventory.getProducts().get(ID).getQuantity());
	}

	@Test
	void view_full_inventory_of_store_inventory() {
		storeInventory.addProduct(NAME, ID, PRICE, QUANTITY);
		storeInventory.viewCurrentInventory();
		String expected = "Product name: " + NAME + "Product id: " + ID + "Product price: " + PRICE
				+ "Product quantity: " + QUANTITY;
		String actual = storeInventory.viewCurrentInventory();
		assertEquals(expected, actual);
	}

	@Test
	void calculate_total_value_of_inventory() {
		storeInventory.addProduct(NAME, ID, PRICE, QUANTITY);
		double expected = PRICE * QUANTITY;
		double actual = storeInventory.calculateTotalValue();
		assertEquals(expected, actual);

	}
}
