import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductTest {

	public static final String NAME = "Product Name";
	public static final int ID = 1234;
	public static final double PRICE = 99.99;
	public static final int QUANTITY = 5;

	Product product;

	@BeforeEach
	void setup() {
		product = new Product(NAME, ID, PRICE, QUANTITY);
	}

	@Test
	void product_name_matches_supplied_name() {
		assertEquals(NAME, product.getName());
	}

	@Test
	void product_id_matches_supplied_id() {
		assertEquals(ID, product.getId());
	}

	@Test
	void product_price_matches_supplied_price() {
		assertEquals(PRICE, product.getPrice());
	}

	@Test
	void product_quantity_matches_supplied_quantity() {
		assertEquals(QUANTITY, product.getQuantity());
	}

}
